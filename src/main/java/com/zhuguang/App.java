package com.zhuguang;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.zhuguang.datasource.DBConfig1;
import com.zhuguang.datasource.DBConfig2;

/**
 * 非常感谢腾讯课堂烛光学院的lisa老师
 * @author zhuwen
 *
 */
@SpringBootApplication
//@ComponentScan("com.zhuguang.app")

//@ComponentScan("com.zhuguang.controller")
//@EnableTransactionManagement 
@EnableConfigurationProperties(value = { DBConfig1.class, DBConfig2.class })

@MapperScan(basePackages = { "com.zhuguang.mapper" })

//@EnableAutoConfiguration
//@EnableAsync
//@EnableCaching
public class App {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

}
